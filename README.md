This war includes a Artemis client consumer tested on the RedHat jamq 7.8 broker and EAP 7.2.4.

Remember to assign each consumer with an unique http port.

To build a war:
```bash
./mvnw clean package -Dmaven.test.skip
```
