package com.crv.test.mq.listeners.a;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBContext;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.concurrent.TimeUnit;


@MessageDriven(name = "InternalMigrationEventListener", activationConfig = {
        // https://docs.jboss.org/ejb3/docs/tutorial/1.0.7/html/Message_Driven_Beans.html
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "/jms/queue/internalDEAMigrationQueue"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "clientID", propertyValue = "de-customer-adapter"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")})
public class ListenerA implements MessageListener {
    private static final Logger LOG = LoggerFactory.getLogger(ListenerA.class);

    @Resource
    private EJBContext ejbContext;

    // With this onMessage the messages are moved to the expiryQueue. However, we expect the messages to be removed.
//    @Override
//    public void onMessage(Message message) {
//        // Check if we are in error-state (this stops processing messages and prevents message loss or loss in order when we are unable to process the event)
//        // We have an instant redelivery delay (0) and redelivery indefinitely (-1) configured in the broker.xml to prevent loss in message order
//        // for this test case this if will always be true since that's when we experience this issue.
//        if (true) {
//            try {
//                LOG.info("Received message");
//                // We wait a timeout before rejecting to prevent the event to be offered immediately after rejecting.
//                // this prevents DDOS'ing our own applications when an application is unable to handle the event (throws an error upon processing it)
//                Thread.sleep(TimeUnit.SECONDS.toMillis(5));
//            } catch (InterruptedException e) {
//                LOG.error("Got Exception", e);
//                // LOGGER.error("Our timeout for accessing new events got interrupted. This should never happen. What happens after this is unknown", e);
//                Thread.currentThread()
//                        .interrupt();
//            } finally {
//                LOG.info("Rolling back");
//                ejbContext.setRollbackOnly();
//            }
//        }
//        // Normally we handle have our business logic here
//    }

    // With this onMessage the messages are removed
    @Override
    public void onMessage(Message message) {
        try {
            LOG.info("Received message");
            Thread.sleep(TimeUnit.MINUTES.toMillis(6));
        } catch (InterruptedException e) {
            LOG.error("Got Exception", e);
            Thread.currentThread()
                    .interrupt();
        }
        LOG.info("Processed message");
    }
}
